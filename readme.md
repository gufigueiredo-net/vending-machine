# Vending Machine

Este projeto foi criado como parte do processo seletivo da Minutrade BH, conforme descrito no documento técnico enviado com as instruções para realização do teste.


## Visão geral
Estre projeto tem como objetivo a criação de uma estrutura para integração de controle de Vending Machine (máquina de snacks). Essa estrutura controla o saldo dos cartões, recarga, compra de produtos etc.

## Modelagem do Domínio
  

![](https://s3.amazonaws.com/gfcvending/VendingContext.png)

## Arquitetura

As tecnologias/patterns utilizados foram: 

*  .NET Core, API REST
*  MongoDB
*  DDD
*  CQRS
*  Docker


O padrão **CQRS** (Command Query Responsibility Segregation) é representado através do diagrama abaixo:
![](https://s3.amazonaws.com/gfcvending/cqrs.png)

## Requisitos

O projeto requer as seguintes ferramentas para runtime:

*  Docker >= 18.09
*  Docker-Compose


Para compilação e debug local, é necessário também:

*  [.NET Core SDK 2.2](https://dotnet.microsoft.com/download)
*  MongoDB (local na porta:27017)

_Não é necessário instalar o SDK e Mongo caso for apenas executar o projeto._

## Instalação

Para executar o projeto em runtime, executar o seguinte comando na raiz do projeto:

```sh
$ cd src
$ docker-compose build
$ docker-compose up -d
```


Para visualizar/testar os métodos de API, acesse o swagger através da url [localhost:5000/swagger](http://localhost:5000/swagger)

Caso queira fazer o debug local, após instalado as dependências (SDK e Mongo), executar:

```sh
$ cd src/VendingMachineAPI
$ dotnet restore
$ dotnet run
```

Para parar a execução dos containers, execute:

```sh
$ docker-compose down
```


## Testes unitários
Para executar os testes unitários, executar os comandos abaixo (requer instalação do SDK)

```sh
$ cd src/VendingMachine.UnitTests
$ dotnet restore
$ dotnet build
$ dotnet test
```

## Seedwork
Ao executar o projeto, uma script de seedwork é executado, incluindo assim os dados mínimos necessários para funcionamento da API. São eles:

1.  Inclusão de uma VendingMachine
2.  Inclusão de alguns produtos
3.  Inclusão de 3 cartões (números **1010, 2020 e 3030**)


## Métodos de API

A API de VendingMachine expõe uma série de métodos que fazem todo controle de venda de produtos, controle de estoque e crédito dos cartões. Estes métodos estão listados na tabela abaixo:

| Método | Descrição |
| ------ | ------ |
| `GET` /api/machine | Lista todas as máquinas registradas. Cada máquina possui um código único acessado através da propriedade **MachineCode** |
| `GET` /api/machine/{machineCode}/inventory | Lista todo o estoque de uma máquina |
| `POST` /api/machine/card | Registra um **novo cartão** na máquina|
| `GET` /api/machine/card/{number} | Obtém todos os dados de um **cartão** |
| `GET` /api/machine/card/{number}/balance | Obtém o **saldo** de um cartão |
| `POST` /api/machine/card/insert | **Insere** um cartão na máquina. Caso seja um novo dia, realiza, também, a recarga do mesmo|
| `POST` /api/vending | Cria uma **ordem** (compra) de um **produto** em uma dada máquina. Para criar uma ordem é necessário o **código da máquina**, **código do produto** e o **número do cartão** |
| `GET` /api/vending/history/{card} | Obtém o histórico de compras de um **cartão** |

** Lembrando que para testes da API, a url da mesma estará exposta na porta 5000**

## Next Steps
Os próximos passos para **possiveis melhorias na arquitetura** do projeto seriam:

1.  Quebra dos serviços de **Produto**, **Cliente** e **Cartão** em microserviços
2.  Comunicação entre serviços utilizando mensageria através de eventos (EventBus)
3.  Utilização de **Event Sourcing** para melhor controle/estudo do fluxo de dados


## Créditos
Projeto construído e modelado por [Gustavo Cruz](mailto:gufigueiredo.net@gmail.com)
