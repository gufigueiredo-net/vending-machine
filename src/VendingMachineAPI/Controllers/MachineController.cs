using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using VendingMachineAPI.Commands;
using VendingMachineAPI.Repositories;

namespace VendingMachineAPI.Controllers
{
    /// <summary>
    /// Controla toda a parte de máquinas e seus cadastros
    /// </summary>
    [Route("api/machine")]
    [ApiController]
    public class MachineController : ControllerBase
    {
        private readonly IMediator _bus;
        private readonly ICardRepository _cardRepository;
        private readonly IVendingMachineRepository _vendingRepository;

        public MachineController(IMediator bus, IVendingMachineRepository vendingRepository, 
          ICardRepository cardRepository)
        {
            _bus = bus;
            _cardRepository = cardRepository;
            _vendingRepository = vendingRepository;
        }

        /// <summary>
        /// Retorna todas as máquinas registradas
        /// </summary>
        [HttpGet]
        public IActionResult Get()
        {
            var result = _vendingRepository.GetAll();
            return Ok(result);
        }

        /// <summary>
        /// Registra um novo cartão. O número do cartão deve ter 4 digitos.
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost("card")]
        public async Task<IActionResult> RegisterCard([FromBody] RegisterCardCommand command)
        {
            command.Validate();

            var response = await _bus.Send(command);
            if (response.ResultCode > 0)
            {
              return BadRequest(response);
            }

            return Accepted();
        }

        /// <summary>
        /// Obtém as informações de um cartão
        /// </summary>
        /// <param name="cardNumber"></param>
        /// <returns></returns>
        [HttpGet("card/{cardNumber}")]
        public IActionResult GetCardInfo(string cardNumber)
        {
            var result = _cardRepository.GetByNumber(cardNumber);
            return Ok(result);
        }

        /// <summary>
        /// Insere um cartão na máquina
        /// </summary>
        /// <param name="command">InsertCardCommand</param>
        [HttpPost("card/insert")]
        public async Task<IActionResult> InsertCard([FromBody] InsertCardCommand command)
        {
            command.Validate();

            var response = await _bus.Send(command);
            if (response.ResultCode > 0)
            {
              return BadRequest(response);
            }

            return Accepted();
        }
    }
}
