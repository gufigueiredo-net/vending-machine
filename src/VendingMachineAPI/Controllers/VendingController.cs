﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using VendingMachineAPI.Commands;
using VendingMachineAPI.Repositories;

namespace VendingMachineAPI.Controllers {

  /// <summary>
  /// Controla a parte de vendas/pedidos das máquinas
  /// </summary>
  [Route ("api/vending")]
  [ApiController]
  public class VendingController : ControllerBase {
    private readonly IMediator _bus;
    private readonly ICardRepository _cardRepository;
    private readonly IVendingOrderRepository _vendingOrderRepository;

    public VendingController (IMediator bus, ICardRepository cardRepository,
      IVendingOrderRepository vendingOrderRepository) {
      
      _cardRepository = cardRepository;
      _vendingOrderRepository = vendingOrderRepository;
      _bus = bus;
    }

    /// <summary>
    /// Obtém o histórico de compras de um cartão
    /// </summary>
    /// <param name="cardNumber">Número do cartão</param>
    [HttpGet("history/{cardNumber}")]
    public IActionResult GetOrderHistory(string cardNumber) {
      var card = _cardRepository.GetByNumber(cardNumber);
      if (card == null)
        return BadRequest("This card does not exists");
      
      var history = _vendingOrderRepository.GetOrders(card.Id);
      return Ok(history);
    }

    /// <summary>
    /// Cria uma ordem de compra de produto
    /// </summary>
    /// <param name="command">CreateVendingOrderCommand</param>
    [HttpPost]
    public async Task<IActionResult> Post ([FromBody] CreateVendingOrderCommand command) {
      var response = await _bus.Send (command);
      if (response.ResultCode > 0) {
        return BadRequest (response);
      }

      return Accepted ();
    }
  }
}