using System.Linq;
using MongoDB.Driver;
using Microsoft.Extensions.Configuration;
using VendingMachineAPI.DomainCore;

namespace VendingMachineAPI.Infrastructure
{
    public class VendingMachineContext : IDbContext
    {
        private IConfiguration _configuration;
        public VendingMachineContext(IConfiguration configuration)
        {
            this._configuration = configuration;
        }

        public IMongoCollection<T> GetCollection<T>(string name) 
        {
              MongoClient client = new MongoClient(
                  _configuration.GetConnectionString("MongoConnection"));
              IMongoDatabase db = client.GetDatabase("vendingMachineDB");

              return db.GetCollection<T>(name);
        }

        public T Insert<T>(T instance) where T: Entity
        {
          var collection = GetCollection<T>(typeof(T).Name);
          collection.InsertOne(instance);
          return instance;
        }

        public void Update<T>(T instance) where T: Entity
        {
          var collection = GetCollection<T>(typeof(T).Name);
          var filter = Builders<T>.Filter.Eq("_id", instance.Id);
          collection.FindOneAndReplace(filter, instance);
        }

        public void Delete<T>(string id) where T: Entity
        {
          var collection = GetCollection<T>(typeof(T).Name);
          var filter = Builders<T>.Filter.Eq("_id", id);
          collection.DeleteOne(filter);
        }
  }
}