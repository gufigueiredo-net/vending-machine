using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using VendingMachineAPI.Domain;

namespace VendingMachineAPI.Infrastructure
{
    public class SeedworkContext
    {
        private IConfiguration _configuration;
        private IMongoDatabase _db;
        public SeedworkContext(IConfiguration configuration)
        {
            this._configuration = configuration;
            MongoClient client = new MongoClient(
                _configuration.GetConnectionString("MongoConnection"));
            client.DropDatabase("vendingMachineDB");
            _db = client.GetDatabase("vendingMachineDB");
        }

        public void Seed()
        {
          //Create Collections
          _db.CreateCollection("Product");
          _db.CreateCollection("VendingMachine");
          _db.CreateCollection("Customer");
          _db.CreateCollection("Card");
          _db.CreateCollection("CardChargeHistory");
          _db.CreateCollection("VendingOrder");

          //Products
          List<Product> products = new List<Product>()
          {
            new Product(Guid.NewGuid().ToString(), "Doritos Nacho", "DNACH", 5.5M),
            new Product(Guid.NewGuid().ToString(), "Ruffles", "RUFFL", 5.5M),
            new Product(Guid.NewGuid().ToString(), "Cheetos Requeijão", "CHREQ", 4.5M),
            new Product(Guid.NewGuid().ToString(), "Biscoito Oreo", "BSORE", 3M),
            new Product(Guid.NewGuid().ToString(), "Chocolate KitKat", "CHKIT", 3.5M),
            new Product(Guid.NewGuid().ToString(), "Chocolate Laka", "CHLAK", 3M),
            new Product(Guid.NewGuid().ToString(), "Chiclete Trident", "CLTRI", 2M),
            new Product(Guid.NewGuid().ToString(), "Bala Halls", "BLHAL", 2M),
            new Product(Guid.NewGuid().ToString(), "Toddynho", "BDTODY", 3M),
            new Product(Guid.NewGuid().ToString(), "Gatorade", "BDGATR", 4.5M),
            new Product(Guid.NewGuid().ToString(), "Coca-Cola Lata", "BDCOKE", 4M),
            new Product(Guid.NewGuid().ToString(), "Sprite Lata","BDSPRI", 4M),
            new Product(Guid.NewGuid().ToString(), "Fanta Laranja Lata", "BDFANT", 4M)
          };

          var productCollection = _db.GetCollection<Product>("Product");
          productCollection.InsertMany(products);

          //Vending Machine and inventory
          var vm = new VendingMachine(Guid.NewGuid().ToString(), 10);
          products.ForEach(p => vm.AddInventory(p));
          vm.Recharge();
          _db.GetCollection<VendingMachine>("VendingMachine").InsertOne(vm);

          //Customers and cards
          var cardCollection = _db.GetCollection<Card>("Card");
          Customer lucio = new Customer("Lucio Santana", "3198576847");
          Card lucioCard = new Card(Guid.NewGuid().ToString(), "1010", lucio);

          Customer lucas = new Customer("Lucas Machado", "3198475820");
          Card lucasCard = new Card(Guid.NewGuid().ToString(), "1010", lucio);

          Customer gustavo = new Customer("Gustavo Cruz", "3192535062");
          Card gustavoCard = new Card(Guid.NewGuid().ToString(), "1010", lucio);

          cardCollection.InsertOne(lucioCard);
          cardCollection.InsertOne(lucasCard);
          cardCollection.InsertOne(gustavoCard);
          
        }
    }
}