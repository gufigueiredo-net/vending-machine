using MongoDB.Driver;
using VendingMachineAPI.DomainCore;

namespace VendingMachineAPI.Infrastructure
{
  public interface IDbContext
  {
    IMongoCollection<T> GetCollection<T>(string name);
    T Insert<T>(T instance) where T: Entity;
    void Update<T>(T instance) where T: Entity;
    void Delete<T>(string id) where T: Entity;
  }
}