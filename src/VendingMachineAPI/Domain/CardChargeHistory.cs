using VendingMachineAPI.DomainCore;
using System;

namespace VendingMachineAPI.Domain
{
    public class CardChargeHistory : Entity
    {
        public string CardNumber { get; private set; }
        public decimal Amount { get; private set; }
        public DateTime Date {get; private set; }

        public CardChargeHistory(string id, string cardNumber, decimal amount)
        {
            this.Id = id;
            this.CardNumber = cardNumber;
            this.Amount = amount;
            this.Date = DateTime.Now;
        }
    }
}