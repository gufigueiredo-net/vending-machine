using System;
using VendingMachineAPI.DomainCore;

namespace VendingMachineAPI.Domain
{
    public class Product : Entity
    {
        public string Name { get; private set; }
        public string Code { get; private set;}
        public decimal Price { get; private set; }

        public Product(string id, string name, string code, decimal price)
        {
            this.Id = id;

            if (string.IsNullOrWhiteSpace(name)) {
              throw new Exception("Product must have a name");
            }
            this.Name = name;

            if (string.IsNullOrWhiteSpace(code)) {
              throw new Exception("Product must have a code");
            }
            this.Code = code;

            if (price <= 0) {
              throw new Exception("Product price must be greater than zero");
            }
            this.Price = price;
        }
    }
}