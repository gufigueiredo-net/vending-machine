using System;
using VendingMachineAPI.DomainCore;

namespace VendingMachineAPI.Domain
{
    public class Card : Entity, IAggregateRoot
    {
        public string Number { get; private set; }
        public decimal CreditBalance { get; private set; }
        public Customer Customer { get; private set; }

        public Card(string id, string number, Customer customer)
        {
            if (customer == null)
            {
              throw new Exception("Customer cannot be null");
            }

            if (number.Length != 4)
            {
              throw new Exception("Card number must have 4 digits");
            }

            this.Id = id;
            this.Customer = customer;
            this.Number = number;
        }

        public bool CanBuyItem(decimal itemPrice)
        {
          return this.CreditBalance > itemPrice;
        }

        public void Recharge(decimal value) 
        {
          if (value <= 0) {
            throw new Exception("Recharge value must be greather than zero");
          }
          this.CreditBalance = value;
          //lancar evento
        }

        public void DebitValue(decimal value) 
        {
          if (value > this.CreditBalance) {
            throw new Exception("There is no available balance for this purchase");
          }
          this.CreditBalance -= value;
          //lancar evento

        }
    }
}