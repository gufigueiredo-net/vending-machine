using System;
using VendingMachineAPI.DomainCore;

namespace VendingMachineAPI.Domain
{
    public class Customer
    {
        public string Name { get; private set; }
        public string PhoneNumber { get; private set; }
        public Customer(string name, string phoneNumber)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
              throw new Exception("Customer name cannot be null or empty");
            }
            this.Name = name;
            this.PhoneNumber = phoneNumber;
        }
    }
}