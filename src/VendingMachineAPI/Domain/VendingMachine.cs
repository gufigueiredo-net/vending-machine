using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using VendingMachineAPI.DomainCore;

namespace VendingMachineAPI.Domain
{
    public class VendingMachine : Entity, IAggregateRoot
    {
        public string MachineCode { get; private set; }
        public int MaxItemsPerSlot { get; private set; }
        public List<ProductStock> Inventory { get; set; }
        public VendingMachine(string id, int maxItemsPerSlot)
        {
          this.Id = id;
          this.MaxItemsPerSlot = maxItemsPerSlot;
          this.MachineCode = Path.GetRandomFileName().Replace(".", "").Substring(0, 6);
        }

        /// <summary>
        /// Adiciona um produto ao inventory da máquina
        /// </summary>
        public void AddInventory(Product product) {
            if (this.Inventory == null)
              this.Inventory = new List<ProductStock>();
            this.Inventory.Add(new ProductStock(product.Id, 0));
        }

        /// <summary>
        /// Retira um produto do slot da máquina
        /// </summary>
        public void DropInventoryItem(string productId) {
          var stock = this.Inventory.FirstOrDefault(p => p.ProductId == productId);
          
          if (stock == null)
            throw new Exception("This product doesn't exists in the machine inventory");
          
          if (stock.OutOfStock())
            throw new OutOfStockException();
          
          stock.DropItem();
        }

        /// <summary>
        /// Recarrega a máquina com base no máximo de itens por slot
        /// </summary>
        public void Recharge() {
          this.Inventory.ForEach(p => p.RefreshStock(this.MaxItemsPerSlot));
        }

        /// <summary>
        /// Recarrega um produto especifico da máquina com base no máximo de itens por slot
        /// </summary>
        public void Recharge(string productId)
        {
          var stock = this.Inventory.FirstOrDefault(p => p.ProductId == productId);

          if (stock == null)
            throw new Exception("This product doesn't exists in the machine inventory");
          stock.RefreshStock(this.MaxItemsPerSlot);
        }
    }

    public class OutOfStockException : Exception
    {
    }
}