using System;

namespace VendingMachineAPI.Domain
{
    public class ProductStock
    {
        public string ProductId { get; private set; }
        public int AvailableItems { get; private set; }

        public ProductStock(string productId, int itemQty)
        {
            this.ProductId = productId;
            RefreshStock(itemQty);
        }

        public bool OutOfStock() {
          return AvailableItems == 0;
        }

        public void RefreshStock(int qty) {
          if (qty < 0)
            throw new Exception("Stock must be greater or equal to zero");
          this.AvailableItems = qty;
        }
        public void DropItem() {
          if (this.AvailableItems == 0)
            return;
          this.AvailableItems--;
        }
    }
}