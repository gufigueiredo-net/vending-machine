using System;
using VendingMachineAPI.DomainCore;

namespace VendingMachineAPI.Domain
{
    public class VendingOrder : Entity
    {
        public string CardId { get; private set; }
        public string ProductId { get; private set; }
        public string MachineId { get;private set; }
        public string Customer { get; private set; }
        public string Product { get; private set; }
        public DateTime Date { get; set; }
        public decimal OrderValue { get; private set; }

        public VendingOrder(string id, string machineId, Card buyer, Product product)
        {
            this.Id = id;
            this.MachineId = machineId;
            this.CardId = buyer.Id;
            this.Customer = buyer.Customer.Name;
            this.ProductId = product.Id;
            this.Product = product.Name;
            this.OrderValue = product.Price;
        }

    }
}