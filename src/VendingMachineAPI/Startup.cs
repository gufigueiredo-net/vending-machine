﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Swagger;
using VendingMachineAPI.CommandHandlers;
using VendingMachineAPI.Commands;
using VendingMachineAPI.Infrastructure;
using VendingMachineAPI.Repositories;

namespace VendingMachineAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //MongoDB Context
            services.AddTransient<IDbContext, VendingMachineContext>();
            services.AddTransient<SeedworkContext>();

            //Repositories
            services.AddScoped<IVendingMachineRepository, VendingMachineRepository>();
            services.AddScoped<ICardRepository, CardRepository>();
            services.AddScoped<IProductRepository, ProductRepository>();
            services.AddScoped<IVendingOrderRepository, VendingOrderRepository>();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            //Service Bus
            services.AddMediatR();

            //Swagger UI
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { 
                  Title = "VendingMachineAPI",
                  Description = "API de integração para máquinas de snack",
                  Version = "v1" 
                });
                var filePath = Path.Combine(AppContext.BaseDirectory, "VendingMachineAPI.xml");
                c.IncludeXmlComments(filePath);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //Configure CORS
            app.UseCors(c =>
            {
                c.AllowAnyHeader();
                c.AllowAnyMethod();
                c.AllowAnyOrigin();
            });

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "VendingMachineAPI v1");
            });

            app.UseMvc();
        }
    }
}
