using System.Collections.Generic;
using VendingMachineAPI.Domain;

namespace VendingMachineAPI.Repositories
{
    public interface IVendingMachineRepository
    {
        VendingMachine GetByCode(string machineCode);
        IEnumerable<VendingMachine> GetAll();
    }
}