using VendingMachineAPI.Domain;
using VendingMachineAPI.Infrastructure;
using System.Linq;
using System.Collections.Generic;
using MongoDB.Driver;

namespace VendingMachineAPI.Repositories
{
    public class VendingMachineRepository : IVendingMachineRepository
    {
        private readonly IDbContext _context;

        public VendingMachineRepository(IDbContext context)
        {
          this._context = context;
        }
        public VendingMachine GetByCode(string machineCode)
        {
          return _context.GetCollection<VendingMachine>("VendingMachine")
                         .Find(m => m.MachineCode == machineCode)
                         .FirstOrDefault();
        }

        public IEnumerable<VendingMachine> GetAll()
        {
          return _context.GetCollection<VendingMachine>("VendingMachine").AsQueryable().ToList();
        }
  }
}