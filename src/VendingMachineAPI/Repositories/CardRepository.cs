using VendingMachineAPI.Domain;
using VendingMachineAPI.Infrastructure;
using System.Linq;
using System.Collections.Generic;
using MongoDB.Driver;

namespace VendingMachineAPI.Repositories
{
    public class CardRepository : ICardRepository
    {
        private readonly IDbContext _context;

        public CardRepository(IDbContext context)
        {
          this._context = context;
        }

        public Card GetByNumber(string cardNumber) 
        {
          return _context.GetCollection<Card>("Card").Find(c => c.Number == cardNumber).FirstOrDefault();
        }

        public CardChargeHistory GetLastCharge(string cardNumber)
        {
          var sort = Builders<CardChargeHistory>.Sort.Descending("Date");
          var filter = Builders<CardChargeHistory>.Filter.Eq("CardNumber", cardNumber);

          return _context.GetCollection<CardChargeHistory>("CardChargeHistory")
                         .Find(filter)
                         .Sort(sort)
                         .Limit(1)
                         .FirstOrDefault();
        }
    }
}