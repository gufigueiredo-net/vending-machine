using System.Collections.Generic;
using VendingMachineAPI.Domain;

namespace VendingMachineAPI.Repositories
{
    public interface IVendingOrderRepository
    {
         IEnumerable<VendingOrder> GetOrders(string cardId);
    }
}