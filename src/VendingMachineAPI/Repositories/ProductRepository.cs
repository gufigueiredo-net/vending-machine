using VendingMachineAPI.Domain;
using VendingMachineAPI.Infrastructure;
using System.Linq;
using System.Collections.Generic;
using MongoDB.Driver;

namespace VendingMachineAPI.Repositories
{
  public class ProductRepository : IProductRepository
  {
    private readonly IDbContext _context;

    public ProductRepository(IDbContext context)
    {
      _context = context;
    }
    public Product GetByCode(string productCode)
    {
      return _context.GetCollection<Product>("Product").Find(c => c.Code == productCode).FirstOrDefault();
    }
  }
}