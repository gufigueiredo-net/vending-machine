using VendingMachineAPI.Domain;
using VendingMachineAPI.Infrastructure;
using System.Linq;
using System.Collections.Generic;
using MongoDB.Driver;

namespace VendingMachineAPI.Repositories
{
  public class VendingOrderRepository : IVendingOrderRepository
  {
    private readonly IDbContext _context;

    public VendingOrderRepository(IDbContext context)
    {
      _context = context;
    }
    public IEnumerable<VendingOrder> GetOrders(string cardId)
    {
      return _context.GetCollection<VendingOrder>("VendingOrder")
                     .AsQueryable().Where(v => v.CardId == cardId).ToList();
    }
  }
}