using VendingMachineAPI.Domain;

namespace VendingMachineAPI.Repositories
{
    public interface IProductRepository
    {
         Product GetByCode(string productCode);
    }
}