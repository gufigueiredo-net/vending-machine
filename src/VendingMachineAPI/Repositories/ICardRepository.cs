using VendingMachineAPI.Domain;

namespace VendingMachineAPI.Repositories
{
    public interface ICardRepository
    {
         Card GetByNumber(string cardNumber);
         CardChargeHistory GetLastCharge(string cardNumber);
    }
}