using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using MongoDB.Bson;
using MongoDB.Driver;
using VendingMachineAPI.Commands;
using VendingMachineAPI.Domain;
using VendingMachineAPI.DomainCore;
using VendingMachineAPI.Infrastructure;
using VendingMachineAPI.Repositories;

namespace VendingMachineAPI.CommandHandlers {
  public class VendingCommandHandler : IRequestHandler<CreateVendingOrderCommand, CommandResult> {
    private readonly IDbContext _context;
    private readonly ICardRepository _cardRepository;
    private readonly IVendingMachineRepository _machineRepository;
    private readonly IProductRepository _productRepository;

    public VendingCommandHandler (IDbContext context, ICardRepository cardRepository,
      IVendingMachineRepository machineRepository, IProductRepository productRepository) {
      
      _context = context;
      _productRepository = productRepository;
      _cardRepository = cardRepository;
      _machineRepository = machineRepository;
    }
    public Task<CommandResult> Handle (CreateVendingOrderCommand request, CancellationToken cancellationToken) {
      
      try {

        //Busca o cartão
        var card = _cardRepository.GetByNumber (request.CardNumber);
        if (card == null)
          return Task.FromResult (CommandResult.Error (31, "Cannot proccess order: This card does not exists"));

        //Busca a máquina com seu inventory
        var machine = _machineRepository.GetByCode(request.MachineCode);
        if (machine == null)
          return Task.FromResult (CommandResult.Error (32, "Cannot proccess order: This machine is not registered"));

        //Busca o produto
        var product = _productRepository.GetByCode(request.ProductCode);
        if (product == null)
          return Task.FromResult(CommandResult.Error (33, "Cannot proccess order: Product not registered"));

        //Busca o produto no slot da máquina
        var stock = machine.Inventory.FirstOrDefault (p => p.ProductId == product.Id);
        if (stock == null)
          return Task.FromResult(CommandResult.Error (34, "Cannot proccess order: Product does not exist in the machine inventory"));

        //Verifica se o cartão possui saldo para compra do produto escolhido
        if (!card.CanBuyItem(product.Price))
          return Task.FromResult(CommandResult.Error (37, "Cannot proccess order: Insufficient credit"));

        //Retira o item do slot
        machine.DropInventoryItem(product.Id);

        //Atualiza o crédito do cartao
        card.DebitValue(product.Price);

        //Cria uma nova ordem
        var order = new VendingOrder(Guid.NewGuid().ToString(), machine.Id, card, product);

        //Grava as entidades
        _context.Update<VendingMachine>(machine);
        _context.Update<Card>(card);
        _context.Insert<VendingOrder>(order);

        return Task.FromResult (new CommandResult (0, "Ok"));
      }
      catch (OutOfStockException) 
      {
        return Task.FromResult(CommandResult.Error (38, "Cannot proccess order: Product out of stock"));
      }
    }
  }
}