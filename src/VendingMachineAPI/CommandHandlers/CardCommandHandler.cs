using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using VendingMachineAPI.Commands;
using VendingMachineAPI.Domain;
using VendingMachineAPI.DomainCore;
using VendingMachineAPI.Infrastructure;
using VendingMachineAPI.Repositories;

namespace VendingMachineAPI.CommandHandlers
{
  public class CardCommandHandler : 
      IRequestHandler<InsertCardCommand, CommandResult>,
      IRequestHandler<RegisterCardCommand, CommandResult>
  {
    private const decimal DAILY_AMOUNT_CHARGE = 50M;
    private readonly IDbContext _context;
    private readonly ICardRepository _cardRepository;
    private readonly IVendingMachineRepository _machineRepository;

    public CardCommandHandler(IDbContext context, ICardRepository cardRepository, 
    IVendingMachineRepository machineRepository)
    {
      _context = context;
      _cardRepository = cardRepository;
      _machineRepository = machineRepository;
    }
    public Task<CommandResult> Handle(InsertCardCommand request, CancellationToken cancellationToken)
    {
      //Valida se o cartão e a máquina estão registrados
      var card = _cardRepository.GetByNumber(request.CardNumber);
      if (card == null)
        return Task.FromResult(CommandResult.Error(21, "This card does not exists"));

      var machine = _machineRepository.GetByCode(request.MachineCode);
      if (machine == null)
        return Task.FromResult(CommandResult.Error(11, "Vending machine not registered"));
      
      //Busca a ultima recarga do cartao e recarrega se novo dia
      var lastCharge = _cardRepository.GetLastCharge(request.CardNumber);
      if (lastCharge == null || (lastCharge != null && lastCharge.Date.Date < DateTime.Now.Date))
      {
        //Insere o historico
        var charge = new CardChargeHistory(Guid.NewGuid().ToString(), request.CardNumber, DAILY_AMOUNT_CHARGE);
        _context.Insert<CardChargeHistory>(charge);

        //Atualiza o saldo do cartão
        card.Recharge(DAILY_AMOUNT_CHARGE);
        _context.Update<Card>(card);
      }

      return Task.FromResult(CommandResult.Ok());
    }

    public Task<CommandResult> Handle(RegisterCardCommand request, CancellationToken cancellationToken)
    {
      //Verifica se o cartão existe
      var card = _cardRepository.GetByNumber(request.CardNumber);
      if (card != null)
        return Task.FromResult(CommandResult.Error(23, $"The card '{request.CardNumber}' is already taken"));

      //Insere o cartão
      var customer = new Customer(request.CustomerName, request.CustomerPhoneNumber);
      var newCard = new Card(Guid.NewGuid().ToString(), request.CardNumber, customer);
      _context.Insert<Card>(newCard);

      return Task.FromResult(CommandResult.Ok());
    }
  }
}