namespace VendingMachineAPI.DomainCore
{
    public interface ICommand
    {
         void Validate();
    }
}