namespace VendingMachineAPI.DomainCore
{
    public class CommandResult
    {
        public CommandResult()
        {
        }
        public CommandResult(int resultCode, string message) {
          this.ResultCode = resultCode;
          this.Message = message;
        }

        public static CommandResult Ok() {
          return new CommandResult(0, "Ok");
        }

        public static CommandResult Error(int resultCode, string message) {
          return new CommandResult(resultCode, message);
        }

        public int ResultCode { get; set; }
        public string Message { get; set; }
    }
}