using System;
using MediatR;
using VendingMachineAPI.DomainCore;

namespace VendingMachineAPI.Commands
{
  public class RegisterCardCommand : ICommand, IRequest<CommandResult>
  {
    public string CustomerName { get;set;}
    public string CustomerPhoneNumber {get;set;}
    public string MachineCode {get;set;}
    public string CardNumber {get;set;}

    public void Validate()
    {
      if (string.IsNullOrWhiteSpace(CustomerName))
        throw new Exception("Customer name must be specified");
      if (string.IsNullOrWhiteSpace(MachineCode))
        throw new Exception("Machine Code must be specified");
      if (string.IsNullOrWhiteSpace(CardNumber))
        throw new Exception("Card Number must be specified");
      if (CardNumber.Length != 4)
        throw new Exception("Card number must have lenght of 4 chars");
    }
  }
}