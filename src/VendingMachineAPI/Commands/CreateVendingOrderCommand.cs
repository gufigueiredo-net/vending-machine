using System;
using MediatR;
using VendingMachineAPI.DomainCore;

namespace VendingMachineAPI.Commands
{
  public class CreateVendingOrderCommand : ICommand, IRequest<CommandResult>
  {
    public string MachineCode { get; set; }
    public string CardNumber { get; set; }
    public string ProductCode { get; set; }
    public void Validate()
    {
      if (string.IsNullOrWhiteSpace(this.MachineCode))
        throw new Exception("MachineID must be specified");
      if (string.IsNullOrWhiteSpace(this.CardNumber))
        throw new Exception("MachineID must be specified");
      if (string.IsNullOrWhiteSpace(this.ProductCode))
        throw new Exception("MachineID must be specified");
    }
  }
}