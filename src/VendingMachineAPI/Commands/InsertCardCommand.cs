
using System;
using MediatR;
using VendingMachineAPI.DomainCore;

namespace VendingMachineAPI.Commands
{
  public class InsertCardCommand : ICommand, IRequest<CommandResult>
  {
    public string CardNumber { get; set; }
    public string MachineCode { get; set; }
    public void Validate()
    {
      if (string.IsNullOrWhiteSpace(CardNumber))
        throw new Exception("Card number must be specified");
      if (string.IsNullOrWhiteSpace(MachineCode))
        throw new Exception("Machine code must be specified");
    }
  }
}