using System;
using VendingMachineAPI.Domain;
using Xunit;

namespace VendingMachine.UnitTests.Domain
{
    public class ProductStockTest
    {
        [Fact]
        public void Should_Create_Product_Stock_With_Available_Items()
        {
          var stock = new ProductStock("123456", 10);
          Assert.Equal(stock.AvailableItems, 10);
        }

        [Fact]
        public void Out_of_Stock_Should_Be_True_When_No_Availabe_Items(){
          var stock = new ProductStock("123456", 0);
          var isItemsAvailable = stock.OutOfStock();
          Assert.True(isItemsAvailable);
        }

        [Fact]
        public void Shoud_Throw_Exception_When_Refresh_Negative_Stock()
        {
          var stock = new ProductStock("123456", 0);
          var ex = Assert.Throws<Exception>(() => stock.RefreshStock(-1));
          Assert.IsType(typeof(Exception), ex);
        }

        [Fact]
        public void Should_Refresh_Stock_Items()
        {
          var stock = new ProductStock("123456", 5);
          Assert.Equal(stock.AvailableItems, 5);
        }
        [Fact]
        public void Should_Drop_Item(){
          var stock = new ProductStock("123456", 10);
          stock.DropItem();
          Assert.Equal(stock.AvailableItems, 9);
        }

        [Fact]
        public void When_Available_Items_Is_Zero_Should_Not_Drop_More_Items(){
          var stock = new ProductStock("123456", 0);
          stock.DropItem();
          Assert.Equal(stock.AvailableItems, 0);
        }
    }
}