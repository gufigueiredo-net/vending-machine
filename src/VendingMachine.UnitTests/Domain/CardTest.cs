using System;
using VendingMachineAPI.Domain;
using Xunit;

namespace VendingMachine.UnitTests.Domain
{
    public class CardTest
    {
        private readonly Customer customer;

        public CardTest()
        {
            this.customer = new Customer("Fake Customer", "3199999999");
        }
        [Fact]
        public void Should_Ensure_4_Digits_Card()
        {
          var ex = Assert.Throws<Exception>(() => new Card(Guid.NewGuid().ToString(), "22", this.customer));
          Assert.IsType(typeof(Exception), ex);
        }

        [Fact]
        public void When_Created_Should_Have_Zero_Credit()
        {
          Card card = new Card(Guid.NewGuid().ToString(), "1010", this.customer);
          Assert.True(card.CreditBalance == 0);
        }

        [Fact]
        public void Should_Avoid_Buy_Item_If_Insufficient_Credit()
        {
          Card card = new Card(Guid.NewGuid().ToString(), "1010", this.customer);
          Product product = new Product(Guid.NewGuid().ToString(), "Ruffles", "RUFFL", 4.5M);
          var canBuy = card.CanBuyItem(product.Price);
          Assert.False(canBuy);
        }

        [Fact]
        public void Should_Recharge_Credit_Balance()
        {
          var rechargeValue = 50;
          Card card = new Card(Guid.NewGuid().ToString(), "1010", this.customer);
          card.Recharge(rechargeValue);
          Assert.Equal(rechargeValue, card.CreditBalance);
        }

        [Fact]
        public void Should_Debit_Value()
        {
          var rechargeValue = 50;
          Card card = new Card(Guid.NewGuid().ToString(), "1010", this.customer);
          card.Recharge(rechargeValue);
          card.DebitValue(10);
          Assert.Equal(40, card.CreditBalance);
        }
    }
}