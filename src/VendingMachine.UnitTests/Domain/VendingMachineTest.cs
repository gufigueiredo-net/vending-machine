using System;
using System.Linq;
using VendingMachineAPI.Domain;
using Xunit;
using Machine = VendingMachineAPI.Domain.VendingMachine;

namespace VendingMachine.UnitTests.Domain
{
    public class VendingMachineTest
    {
        [Fact]
        public void Should_Create_Machine_With_Code()
        {
          var machine = new Machine(Guid.NewGuid().ToString(), 10);
          Assert.NotNull(machine.MachineCode);
          Assert.True(machine.MachineCode.Length > 0);
        }

        [Fact]
        public void Should_Create_Machine_With_Max_Slot_Items()
        {
          var machine = new Machine(Guid.NewGuid().ToString(), 10);
          Assert.Equal(machine.MaxItemsPerSlot, 10);
        }

        [Fact]
        public void Should_Add_Product_To_Inventory()
        {
          var machine = new Machine(Guid.NewGuid().ToString(), 10);
          var product = new Product(Guid.NewGuid().ToString(), "Ruffles", "RUFFL", 5M);
          machine.AddInventory(product);
          Assert.Collection(machine.Inventory, i => Assert.Contains(product.Id, i.ProductId));
        }

        [Fact]
        public void Should_Drop_Inventory_Item_From_Slot()
        {
          var machine = new Machine(Guid.NewGuid().ToString(), 10);
          var product = new Product(Guid.NewGuid().ToString(), "Ruffles", "RUFFL", 5M);
          machine.AddInventory(product);
          machine.Recharge(product.Id);
          
          var stock = machine.Inventory.FirstOrDefault(p => p.ProductId == product.Id);
          Assert.Equal(stock.AvailableItems, machine.MaxItemsPerSlot);

          machine.DropInventoryItem(product.Id);

          Assert.Equal(stock.AvailableItems, (machine.MaxItemsPerSlot - 1));
        }

        [Fact] void When_There_Is_No_Items_In_Slot_Should_Throw_Exception_When_Drop_Item()
        {
          var machine = new Machine(Guid.NewGuid().ToString(), 10);
          var product = new Product(Guid.NewGuid().ToString(), "Ruffles", "RUFFL", 5M);
          machine.AddInventory(product);
          Assert.Throws<OutOfStockException>(() => machine.DropInventoryItem(product.Id));
        }

        [Fact]
        public void Should_Full_Recharge_Machine()
        {
          var machine = new Machine(Guid.NewGuid().ToString(), 10);
          
          var product1 = new Product(Guid.NewGuid().ToString(), "Ruffles", "RUFFL", 5M);
          machine.AddInventory(product1);

          var product2 = new Product(Guid.NewGuid().ToString(), "KitKat", "KITK", 4M);
          machine.AddInventory(product2);

          var product3 = new Product(Guid.NewGuid().ToString(), "Halls", "HALL", 3M);
          machine.AddInventory(product3);

          machine.Recharge();

          Assert.All(machine.Inventory, i => Assert.Equal(i.AvailableItems, machine.MaxItemsPerSlot));
        }

    }
}